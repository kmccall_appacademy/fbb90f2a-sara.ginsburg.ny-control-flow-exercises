# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  new_str = ""
  str.chars.each do |char|
    next if char.downcase == char
      new_str += char
    end
  new_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length % 2 == 0
    return str.chars[str.length / 2 - 1] + str.chars[str.length / 2]
  else
    return str.chars[(str.length - 1)/ 2 ]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
   return str.scan(/[aeiou]/).length
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = 1
  (1..num).each {|n| result *= n }
  return result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ""
  if arr.length != 0
    arr.each do |chars|
      next if chars == arr[-1]
        str += chars + separator
    end
    str += arr[-1]
  end
  return str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_str = ""
  str.chars.each_index do |idx|
    if idx % 2 == 0
      new_str += str[idx].downcase
    else
      new_str += str[idx].upcase
    end
  end
  return new_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  new_arr = []
  str.split(" ").each do |word|
    if word.length >= 5
      new_arr.push(reverse(word))
    else
      new_arr.push(word)
    end
  end
  return new_arr.join(" ")

end

def reverse(str)
  rev_arr = []
  str.chars.each {|ch| rev_arr.unshift(ch)}
  return rev_arr.join("")
end
# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizz_buzz_arr = []
  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
      fizz_buzz_arr.push("fizzbuzz")
    elsif num % 3 == 0
      fizz_buzz_arr.push("fizz")
    elsif num % 5 == 0
      fizz_buzz_arr.push("buzz")
    else
      fizz_buzz_arr.push(num)
    end
  end
  return fizz_buzz_arr
end





# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []
  arr.each {|el| new_arr.unshift(el)}
  return new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2..num - 1).each do |n|
    if num % n == 0
      return false
    end
  end
  return true
  

end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).each do |n|
    if num % n == 0
      factors.push(n)
    end
  end
  return factors

end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors = []
  (1..num).each do |n|
    if num % n == 0 && prime?(n)
      prime_factors.push(n)
    end
  end
  return prime_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  return prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even_arr = []
  odd_arr = []
  arr.each do |n|
   if n % 2 == 0
     even_arr.push(n)
   else
     odd_arr.push(n)
   end
 end
 if even_arr.length == 1
   return even_arr[0]
 else
   return odd_arr[0]
 end

end
